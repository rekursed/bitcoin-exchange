import datetime
import xml.etree.ElementTree as ET

import requests


def parse_xml_from_response(xml: str) -> dict:
    tree = ET.fromstring(xml)
    result = list()
    for x in tree.iter('{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}Obs'):
        dict_t = dict()
        y = list(x)
        for item in y:
            if '{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsDimension' in item.tag:
                date = item.attrib.get('value')
                dict_t['date'] = date
            if '{http://www.sdmx.org/resources/sdmxml/schemas/v2_1/data/generic}ObsValue' in item.tag:
                rate = item.attrib.get('value')
                dict_t['rate'] = rate

        result.append(dict_t)

    return result[0]


def main():
    start = str((datetime.datetime.now() - datetime.timedelta(days=30)).date())[:-3]
    ecb_link = f'https://sdw-wsrest.ecb.europa.eu/service/data/EXR/M.GBP.EUR.SP00.A?startPeriod={start}'
    rates_response = requests.get(ecb_link)
    rates = parse_xml_from_response(rates_response.content)
    bitcoin_link = 'https://blockchain.info/ticker'
    bitcoin_response = requests.get(bitcoin_link)
    btc_eur_rate = bitcoin_response.json().get('EUR').get('15m')

    print(float(rates['rate']) * btc_eur_rate)


if __name__ == "__main__":
    main()
