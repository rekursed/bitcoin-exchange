from urllib.parse import urlparse

import requests
from bs4 import BeautifulSoup


def main():
    print('Enter your url')
    url = input()
    try:
        result = urlparse(url)
        response = requests.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            answer = dict()
            answer['domain_name'] = result.netloc
            answer['protocol'] = result.scheme
            answer['title'] = soup.find("title").text
            answer['image'] = list()
            img_list = soup.find_all('img')
            for img in img_list:
                answer['image'].append(img.attrs['src'])
            css_links = soup.find_all("style")
            answer['stylesheets'] = len(css_links)

            print(answer)
        else:
            print('Invalid Url')
    except Exception as e:
        print(f'Invalid Url {e}')


if __name__ == "__main__":
    main()
